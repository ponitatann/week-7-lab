# Week 7 Lab

This is a sample repository.

## How to set up in local

* Clone project from remote
```
git clone git@gitlab.com:ng-paragon-2021/section2/week-7-lab.git 
```

__Note__: please clone the project inside `htdocs` folder.

* Start development stack

* Visit your site `localhost:PORT\week-7-lab`

__Note__: Replace `PORT` with your actual port number

